package config

import (
	"todos/api/models"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func ConnectDB() (*gorm.DB, error) {
	db, err := gorm.Open(sqlite.Open("todos.db"), &gorm.Config{})
	err = db.AutoMigrate(&models.Todos{})

	if err != nil {
		panic("Failed to migrate database")
	}

	return db, nil
}
