package controller

import (
	"net/http"
	"todos/api/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type TodosController struct {
	db *gorm.DB
}

func NewTodosController(db *gorm.DB) *TodosController {
	return &TodosController{db}
}

func (todoscontroller *TodosController) GetAllTodos(c *gin.Context) {
	var todos []models.Todos
	if err := todoscontroller.db.Find(&todos).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "No records found!"})
		return
	}
	c.JSON(http.StatusOK, todos)
}

func (todoscontroller *TodosController) CreateTodos(c *gin.Context) {
	var newTodo models.Todos
	if err := c.ShouldBindJSON(&newTodo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	result := todoscontroller.db.Create(&newTodo)
	if result.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	c.JSON(http.StatusCreated, newTodo)
}

func (todoscontroller *TodosController) FindTodos(c *gin.Context) {
	var newTodo models.Todos
	if err := todoscontroller.db.Where("ID = ?", c.Param("id")).First(&newTodo).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, newTodo)
}

type updateTodoInput struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

func (todoscontroller *TodosController) UpdateTodos(c *gin.Context) {
	id := c.Param("id")

	var todo models.Todos
	var updateTodos updateTodoInput

	if err := todoscontroller.db.First(&todo, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Records not found"})
		return
	}

	if err := c.ShouldBindJSON(&updateTodos); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Invalid request body"})
		return
	}

	if updateTodos.Title != "" {
		todo.Title = updateTodos.Title
	}

	if updateTodos.Description != "" {
		todo.Description = updateTodos.Description
	}

	if updateTodos.Status != "" {
		todo.Status = updateTodos.Status
	}

	if err := todoscontroller.db.Save(&todo).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, todo)
}
