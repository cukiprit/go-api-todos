package main

import (
	"todos/api/config"
	"todos/api/controller"

	"github.com/gin-gonic/gin"
)

func main() {
	db, err := config.ConnectDB()
	if err != nil {
		panic("Failed to connect to database")
	}

	router := gin.Default()
	tc := controller.NewTodosController(db)

	router.GET("/", tc.GetAllTodos)
	router.GET("/todos/:id", tc.FindTodos)
	router.POST("/todos", tc.CreateTodos)
	router.PATCH("/todos/:id", tc.UpdateTodos)

	router.Run("localhost:8080")
}
